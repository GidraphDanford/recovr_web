<?php
include_once "php/session_manager.php";
	// print_r ($_SESSION);
	if(isset($_SESSION['user_name'])){
		header('Location: index.php',true,301);
	}
?>
<!DOCTYPE HTML>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="author" content="iglyphic">
	<link rel="shortcut icon" href="assets/img/logo.png">
	<title>recovR - Login</title>
	<!--Jquery-->
	<script src="vendor/jquery/jquery-3.2.1.min.js" ></script>
	<script src="vendor/hash/hash_functions.js" type="text/javascript" ></script>
	<script src="assets/js/bootbox.min.js"></script>
	<!-- Bootstrap Javascript -->
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/smoothscroll.js"></script>
	<script src="assets/js/wow.min.js"></script>
	<!-- Bootstrap css-->
	<link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/css/bootstrapValidator.min.css"
	/>
	<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.min.js"> </script>
	<!-- Mynav Bar Script -->
	<!-- Custom Javascript -->
	<script src="assets/js/custom.js"></script>
	<!-- reset CSS -->
	<link href="assets/css/reset.css" rel="stylesheet">
	<!-- icon CSS -->
	<link href="assets/css/fontello.css" rel="stylesheet" type="text/css">
	<link href="assets/css/font-awesome.min.css" rel="stylesheet">
	<!-- Main css -->
	<link href="assets/css/main.css" rel="stylesheet" type="text/css">
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
	<script type="text/javascript">
		$(document).ready(function () {
			$('#loginForm').bootstrapValidator({
				message: 'This value is not valid',
				feedbackIcons: {
					valid: 'glyphicon glyphicon-ok',
					invalid: 'glyphicon glyphicon-remove',
					validating: 'glyphicon glyphicon-refresh'
				},
				fields: {
					username: {
						validators: {
							notEmpty: {
								message: "Please provide your email or username"
							}
						}
					},
					password: {
						validators: {
							notEmpty: {
								message: "Please provide your password"
							}
						}
					}
				}
			});
		});
	</script>
</head>

<body class="style-2 nav-on-header login-page">
	<!-- Start Navigation bar -->
	<nav class="navbar">
		<div class="container">
			<!-- Logo -->
			<div class="pull-left">
				<a class="navbar-toggle" href="#" data-toggle="offcanvas">
					<i class="ti-menu"></i>
				</a>
				<div class="logo-wrapper">
					<a class="logo" href="index.php">
						<img src="assets/img/logo.png" width="63px" height="60px" alt="logo">
					</a>
					<a class="logo-alt" href="index.php">
						<img src="assets/img/logo.png" alt="logo-alt">
					</a>
				</div>
			</div>
			<!-- END Logo -->
			<!-- User account -->
			<div class="pull-right user-login">
				<a class="btn btn-sm btn-primary" href="Login.php">Login</a> or &nbsp;
				<a href="Register.php">Register</a>
			</div>
			<!-- END User account -->
			<!-- Navigation menu -->
			<ul class="nav-menu user-login">
				<li>
					<a  href="index.php">Home</a>
				</li>
				<li>
					<a   href="Download.php">Download</a>
				</li>
				<li>
					<a  href="Report.php">Report</a>
				</li>
				<li>
					<a  href="About.php">About Us</a>
				</li>
				<li>
					<a  href="Contact.php">Contact Us</a>
				</li>
			</ul>
			<!-- END Navigation menu -->
		</div>
	</nav>
	<!-- End Navigation bar -->
	<!-- Start Pages Title  -->
	<section id="page-title" class="page-title-style2">
		<div class="color-overlay"></div>
		<div class="container inner-img">
			<div class="row">
				<div class="Page-title">
					<div class="col-md-12 text-center">
						<div class="title-text">
							<h3 style="color:#fff;" class="page-title">recovR - Login</h3>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End Pages Title  -->
	<!-- Content Start -->
	<main class="main-content">
		<div class="login-block">
			<img src="assets/img/logo_.png" alt="">
			<h1>Log into your account</h1>
			<!--<form id="loginForm" method="post">-->
			<form id="loginForm"  accept-charset="utf-8">
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon">
							<i class="ti-email"></i>
						</span>
						<input type="email" name="email" id="email" class="form-control" placeholder="Your Email">
					</div>
				</div>
				<hr class="hr-xs">
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon">
							<i class="ti-unlock"></i>
						</span>
						<input type="password" name="password" id="password" class="form-control" placeholder="Password">
					</div>
				</div>

				<i style="color:red;"></i>
				<!--<div class="login-footer">
				<h6>Or login with</h6>
				<ul class="social-icons">
					<li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
					<li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
					<li><a class="linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>
				</ul>
			</div>-->
			</form>
			<button class="btn btn-primary btn-block" onclick="javascript:signInUser()">Login</button>
		</div>
		<div class="login-links">
			<!--<a class="pull-left" href="#">Forgot Password?</a>-->
			<a class="pull-right" href="Register.php">Register an account</a>
		</div>
	</main>
	<!-- Content End -->
	<!-- start footer area -->
	<section id="index-footer" class="footer-area-content">
		<!-- Newsletter -->
		<div id="newsletter">
			<div class="container">
				<div class="row">
					<div class="col-md-12 text-center">
						<h3>
							<i class="fa fa-envelope-o"></i>Keep in touch, Join our newsletter</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6 col-md-offset-3">
						<div class="col-sm-8">
								<input type="email" required="required" placeholder="Your Email Address" id="subscription_email" class="form-control" name="subscription_email">
						</div>
						<div class="col-sm-4">
							<a href="#" class="btn btn-subscribe">Subscribe</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- END: Newsletter -->
		<div class="footer-bottom footer-wrapper style-3">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="footer-bottom-navigation">
							<div class="cell-view">
								<div class="footer-links">
									<a href="#">Site Map</a>
									<a href="#">Search</a>
									<a href="#">Terms</a>
									<a href="#">Privacy policy</a>
									<a href="#">Contact Us</a>
									<a href="#">Careers</a>
								</div>
								<div class="copyright">All right reserved. &copy;
									<a target="_blank" href="index.php">recovR</a>
									<small> Your Lost Identifiable Items Recovery Experts</small>
								</div>
							</div>
							<div class="cell-view">
								<div class="social-content">
									<a class="post-facebook" href="https://facebook.com/" target="_blank">
										<i class="fa fa-facebook"></i>
									</a>
									<a class="post-google-plus" href="#">
										<i class="fa fa-google-plus"></i>
									</a>
									<a class="post-twitter" href="https://twitter.com/" target="_blank">
										<i class="fa fa-twitter"></i>
									</a>
									<a class="linkedin" href="#">
										<i class="fa fa-linkedin"></i>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- footer area end -->
	<!-- Back to top Link -->
	<div id="to-top" class="main-bg">
		<span class="fa fa-chevron-up"></span>
	</div>
	</body>
	<!--  Notifications Plugin    -->
	<script src="assets/js/bootstrap-notify.js"></script>
	<script type="text/javascript" src="assets/js/notifier.js">	</script>
	<script src="https://www.gstatic.com/firebasejs/4.12.1/firebase.js"></script>
	<script>   // Initialize Firebase
		var config = {
		apiKey: "AIzaSyBMbSNheWTyDk3slRCxO5diLxTynX7Q64Q",
		authDomain: "recovr-app.firebaseapp.com",
		databaseURL: "https://recovr-app.firebaseio.com",
		projectId: "recovr-app",
		storageBucket: "recovr-app.appspot.com",
		messagingSenderId: "327082146500"   };

		firebase.initializeApp(config);

		function signInUser(){
			var email = $('#email').val() || ''
			var password = $('#password').val() || ''

			if(email == ''){
				alert('Login Error: Missing Email Address')
			}else if(password == ''){
				alert('Login Error: Missing Password')
			}else{
				firebase.auth().setPersistence(firebase.auth.Auth.Persistence.SESSION)
				.then(function() {
					firebase.auth().signInWithEmailAndPassword(email, password).then(()=>{
						// window.location.href = 'Index.php'
						var hashed_email = b64_sha1(email.toUpperCase())
						firebase.database().ref('users/'+hashed_email+'/').once('value',(snap)=>{
							if(snap.val()){
								var request;
								var user = snap.val()
								var user_name = user.user_name
								var user_phone = user.user_phone
								var user_token = firebase.auth().currentUser.uid
								var user_firebase_key = hashed_email

								var serializedData = [
									{ name: 'user_name', value: user_name },
									{ name: 'user_email', value: email },
									{ name: 'user_phone', value: user_phone },
									{ name: 'user_token', value: user_token },
									{ name: 'user_firebase_key', value: user_firebase_key }
								]

								request = $.ajax({
									url: 'php/set_session.php',
									contentType: 'application/x-www-form-urlencoded',
									type: 'POST',
									data: serializedData
								})

								request.done((response, textStatus, jqXHR) => {
									console.log(response)
									response = JSON.parse(response)
									console.log('====================================');
									console.log(response);
									console.log('====================================');
									if (response.success == 'true') {
										// alert('tuko true')
										window.location.href = 'dashboard/dashboard.php'
									} else {
										notifier.showNotification('Oooops! The request failed and we are working on the issue right away. Try again in a while','top','right','danger',4000)
									}
								})
								request.fail((jqXHR, textStatus, errorThrown) => {
									notifier.showNotification('Oooops! The request failed and we are working on the issue right away. Try again in a while','top','right','danger',4000)
								})
							}else{
								notifier.showNotification('Oooops! The User doesnt exist','top','left','info',4000)
							}
						})

					}).catch((err)=>{
						console.log(err.message)
						notifier.showNotification('Oooops! '+err.message,'top','right','danger',4000)
					})
				}).catch((err)=> {
					console.log(err.message)
					notifier.showNotification('Oooops! '+err.message,'top','right','danger',4000)
				});
			}
		}
	</script>

</html>
<!-- Localized -->
