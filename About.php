<?php
include_once "php/session_manager.php";
	// print_r ($_SESSION);
?>
<!DOCTYPE HTML>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="author" content="iglyphic">
	<link rel="shortcut icon" href="assets/img/logo.png">
	<title>recovR - Contact Us</title>
	<!-- reset CSS -->
	<link href="assets/css/reset.css" rel="stylesheet">
	<!-- Bootstrap css -->
	<link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<!-- icon CSS -->
	<link href="assets/css/fontello.css" rel="stylesheet" type="text/css">
	<link href="assets/css/font-awesome.min.css" rel="stylesheet">
	<!-- Main css -->
	<link href="assets/css/main.css" rel="stylesheet" type="text/css">
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body class="style-2 nav-on-header">
<!-- Start Navigation bar -->
<nav class="navbar">
	<div class="container">
		<!-- Logo -->
		<div class="pull-left">
			<a class="navbar-toggle" href="#" data-toggle="offcanvas">
				<i class="ti-menu"></i>
			</a>
			<div class="logo-wrapper">
				<a class="logo" href="index.php">
					<img src="assets/img/logo.png" width="63px" height="60px" alt="logo">
				</a>
				<a class="logo-alt" href="index.php">
					<img src="assets/img/logo.png" alt="logo-alt">
				</a>
			</div>
		</div>
		<!-- END Logo -->
		<!-- User account -->
		<div class="pull-right user-login">
			<?php if(isset($_SESSION['user_name'])){
				echo '<a class="btn btn-sm btn-primary" href="Logout.php">Logout</a>';
			}else{
				echo '<a class="btn btn-sm btn-primary" href="Login.php">Login</a> or &nbsp;
							<a href="Register.php">Register</a>';
			} ?>

		</div>
		<!-- END User account -->
		<!-- END User account -->

		<!-- Navigation menu -->
		<ul class="nav-menu user-login">
			<?php if(isset($_SESSION['user_name'])){
				echo '<li><a href="dashboard/dashboard.php">Dashboard</a></li>';
			} ?>
			<li>
				<a  href="index.php">Home</a>
			</li>
      <li>
				<a   href="Download.php">Download</a>
			</li>
			<li>
				<a href="Report.php">Report</a>
			</li>
      <li>
				<a class="active" href="About.php">About Us</a>
			</li>
			<li>
				<a href="Contact.php">Contact Us</a>
			</li>
		</ul>
		<!-- END Navigation menu -->
	</div>
</nav>
<!-- End Navigation bar -->
<!-- Start Pages Title  -->
<section id="page-title" class="page-title-style2">
	<div class="color-overlay"></div>
	<div class="container inner-img">
		<div class="row">
			<div class="Page-title">
				<div class="col-md-12 text-center">
					<div class="title-text">
						<h3 style="color:#fff;" class="page-title">About Us</h3>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- End Pages Title  -->
<!-- Content Start -->
<main class="main-content contact-page-two">
    <!-- Start Contact Us -->
    <section id="best-restaurant" class="best-restaurant">
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center">
            <h2 class="font-lg">About
              <span class="font-semibold chef-red">Us</span>
            </h2>
          </div>
          <!-- col-md-12 -->
        </div>
        <div class="row">
          <div class="col-xs-12 col-sm-6 col-lg-3 col-md-3" style="background:#f7f7f7;height:300px;border-radius:20px;">
            <div class="welcome_wrapper text-left">
              <h5>The company</h5>
              <p>recovR Documents Centre is a fully incorporated local firm dedicated to the effective and efficient recovery
                of lost documents and other properties within the shortest time possible. We are committed to setting and maintaining
                ethical and effective standards in the dynamic recovery of lost documents and other properties, through our service
                team, promotional activities, partnership development, central archival and database.</p>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-lg-3 col-md-3" style="background:#f7f7f7;height:300px;border-radius:20px;">
            <div class="welcome_wrapper text-left">
              <h5>Our Skills</h5>
              <p>We offer a wide range of recovery solutions starting from IDs to Passports to Certificates, Mobile phones, Motor Vehicles,
                Laptops, Licences and so much more, documents safe keeping and data management. We competently help our clients to
                not only recover their lost documents but also provide safe custody services for important documents.</p>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-lg-3 col-md-3" style="background:#f7f7f7;height:300px;border-radius:20px;">
            <div class="welcome_wrapper text-left">
              <h5>Our Technology</h5>
              <p>We employ the best of breed solution from mature and proven technology using various database platforms, successfully
                intergrated into our services to enable us satisfactorily meet our clients' needs in a timely manner that is recorvering
                their lost items within the shortest time possible. We employ the use of ICT to organize our records for effective
                and effecient verification of claims, information management and dissemination.</p>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-lg-3 col-md-3" style="background:#f7f7f7;height:300px;border-radius:20px;">
            <div class="welcome_wrapper text-left">
              <h5>Our Values</h5>
              <p>Our values determine how we treat people, clients, and partners/agents. The values define who we are and how we do
                our work. They help us work together in the most effective way.</p>
              <p>Our Values are: Trustworthy, Committed, Competence, Responsibility, Customer Satisfaction, Team Work, Integrity</p>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- End Contact Us -->
</main>
<!-- Content End -->
<!-- start footer area -->
<section id="index-footer" class="footer-area-content">
	<!-- Newsletter -->
	<div id="newsletter">
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-center">
					<h3><i class="fa fa-envelope-o"></i>Keep in touch, Join our newsletter</h3>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-md-offset-3">
					<div class="col-sm-8">
						<input type="email" required="required" placeholder="Your Email Address" id="email" class="form-control" name="email">
					</div>
					<div class="col-sm-4">
						<a href="#" class="btn btn-subscribe">Subscribe</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- END: Newsletter -->
	<div class="footer-bottom footer-wrapper style-3">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="footer-bottom-navigation">
						<div class="cell-view">
							<div class="footer-links">
								<a href="#">Site Map</a>
								<a href="#">Search</a>
								<a href="#">Terms</a>
								<a href="#">Privacy policy</a>
								<a href="#">Contact Us</a>
								<a href="#">Careers</a>
							</div>
							<div class="copyright">All right reserved. &copy; <a target="_blank" href="index.php">recovR</a><small>   Your Lost Identifiable Items Recovery Experts</small></div>
						</div>
						<div class="cell-view">
							<div class="social-content">
								<a class="post-facebook" href="https://facebook.com/" target="_blank"><i class="fa fa-facebook"></i></a>
								<a class="post-google-plus" href="#"><i class="fa fa-google-plus"></i></a>
								<a class="post-twitter" href="https://twitter.com/" target="_blank"><i class="fa fa-twitter"></i></a>
								<a class="linkedin" href="#"><i class="fa fa-linkedin"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- footer area end -->
<!-- Back to top Link -->
<div id="to-top" class="main-bg"><span class="fa fa-chevron-up"></span></div>
<!--Jquery-->
	<!--Jquery-->
<script src="vendor/jquery/jquery-3.2.1.min.js" ></script>
<script src="vendor/hash/hash_functions.js" type="text/javascript" ></script>
<script src="assets/js/bootbox.min.js"></script>
<!-- Bootstrap Javascript -->
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/smoothscroll.js"></script>
<script src="assets/js/wow.min.js"></script>
<!-- Mynav Bar Script -->
<script src="assets/js/app.min.js"></script>
<!-- Custom Javascript -->
<script src="assets/js/custom.js"></script>
</body>
</html>
<!-- Localized -->
