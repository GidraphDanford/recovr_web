<?php
include_once "php/session_manager.php";
	// print_r ($_SESSION);
?>
<!DOCTYPE HTML>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="author" content="iglyphic">
	<link rel="shortcut icon" href="assets/img/logo.png">
	<title>recovR - Contact Us</title>
	<!-- reset CSS -->
	<link href="assets/css/reset.css" rel="stylesheet">
	<!-- Bootstrap css -->
	<link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<!-- icon CSS -->
	<link href="assets/css/fontello.css" rel="stylesheet" type="text/css">
	<link href="assets/css/font-awesome.min.css" rel="stylesheet">
	<!-- Main css -->
	<link href="assets/css/main.css" rel="stylesheet" type="text/css">
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body class="style-2 nav-on-header">
<!-- Start Navigation bar -->
<nav class="navbar">
	<div class="container">
		<!-- Logo -->
		<div class="pull-left">
			<a class="navbar-toggle" href="#" data-toggle="offcanvas">
				<i class="ti-menu"></i>
			</a>
			<div class="logo-wrapper">
				<a class="logo" href="index.php">
					<img src="assets/img/logo.png" width="63px" height="60px" alt="logo">
				</a>
				<a class="logo-alt" href="index.php">
					<img src="assets/img/logo.png" alt="logo-alt">
				</a>
			</div>
		</div>
		<!-- END Logo -->
		<!-- User account -->
		<div class="pull-right user-login">
			<?php if(isset($_SESSION['user_name'])){
				echo '<a class="btn btn-sm btn-primary" href="Logout.php">Logout</a>';
			}else{
				echo '<a class="btn btn-sm btn-primary" href="Login.php">Login</a> or &nbsp;
							<a href="Register.php">Register</a>';
			} ?>

		</div>
		<!-- END User account -->
		<!-- END User account -->

		<!-- Navigation menu -->
		<ul class="nav-menu user-login">
			<?php if(isset($_SESSION['user_name'])){
				echo '<li><a href="dashboard/dashboard.php">Dashboard</a></li>';
			} ?>
			<li>
				<a  href="index.php">Home</a>
			</li>
			<li>
				<a   href="Download.php">Download</a>
			</li>
			<li>
				<a href="Report.php">Report</a>
			</li>
			<li>
				<a href="About.php">About Us</a>
			</li>
			<li>
				<a class="active" href="Contact.php">Contact Us</a>
			</li>
		</ul>
		<!-- END Navigation menu -->
	</div>
</nav>
<!-- End Navigation bar -->
<!-- Start Pages Title  -->
<section id="page-title" class="page-title-style2">
	<div class="color-overlay"></div>
	<div class="container inner-img">
		<div class="row">
			<div class="Page-title">
				<div class="col-md-12 text-center">
					<div class="title-text">
						<h3 style="color:#fff;" class="page-title">Contact Us</h3>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- End Pages Title  -->
<!-- Content Start -->
<main class="main-content contact-page-two">
				    <!-- Start Contact Us -->
				    <div id="Contact" class="light-wrapper">
				        <div class="container">
				            <div class="inner">
				                <div class="row">
				                    <div class="col-md-8">
				                        <div class="row">
				                            <div class="Contact-Form">
				                                <form class="leave-comment contact-form" method="post" action="#" id="cform" autocomplete="on">
				                                    <div class="Contact-us">
				                                        <div class="form-input col-md-4">
				                                            <input type="text" class="form-control" placeholder="Name">
				                                        </div>
				                                        <div class="form-input col-md-4">
				                                            <input type="email" class="form-control" placeholder="Email">
				                                        </div>
				                                        <div class="form-input col-md-4">
				                                            <input type="text" class="form-control" name="contact_phone" placeholder="Phone">
				                                        </div>
				                                        <div class="form-input col-md-12">
				                                            <textarea class="form-control" rows="5" placeholder="Message"></textarea>
				                                        </div>
				                                        <div class="form-submit col-md-12">
				                                            <button style="color:#FFF; background-color: #EA745D;border: 1px solid #EA745D;" class="btn btn-color" type="submit">Send Message</button>
				                                        </div>
				                                    </div>
				                                </form>
				                            </div>
				                        </div>
				                    </div>
				                    <div class="col-md-4">
				                        <div class="Contact-Info">
				                            <h4>Contact Details</h4>
				                            <div class="tex-contact">
				                                <p>If you have any ideas or questions, please feel free to contact us using our contact data or this form.</p>
				                            </div>
				                            <div class="Block-Contact col-md-6">
				                                <p>Phone :</p>
				                                <ul>
				                                    <li>
				                                        <i class="fa fa-phone"></i>
				                                        <span>+254 701 933 075</span>
				                                    </li>
				                                </ul>
				                                <p>Email :</p>
				                                <ul>
				                                    <li>
				                                        <i class="fa fa-envelope"></i>
				                                        <span>info@recovR.com</span>
				                                    </li>
				                                </ul>
				                            </div>
				                            <div class="Block-Contact col-md-12">
				                                <p>Address :143-10300 Kerugoya</p>
				                                <ul>
				                                    <li>
				                                        <i class="fa fa-map-marker"></i>
				                                        <span>Kirinyaga University, Kutus Town, Kirinyaga.</span>
				                                    </li>
				                                </ul>
				                            </div>
				                        </div>
				                    </div>
				                </div>
				            </div>
				        </div>
				    </div>
				    <!-- End Contact Us -->
</main>
<!-- Content End -->
<!-- start footer area -->
<section id="index-footer" class="footer-area-content">
	<!-- Newsletter -->
	<div id="newsletter">
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-center">
					<h3><i class="fa fa-envelope-o"></i>Keep in touch, Join our newsletter</h3>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-md-offset-3">
					<div class="col-sm-8">
						<input type="email" required="required" placeholder="Your Email Address" id="email" class="form-control" name="email">
					</div>
					<div class="col-sm-4">
						<a href="#" class="btn btn-subscribe">Subscribe</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- END: Newsletter -->
	<div class="footer-bottom footer-wrapper style-3">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="footer-bottom-navigation">
						<div class="cell-view">
							<div class="footer-links">
								<a href="#">Site Map</a>
								<a href="#">Search</a>
								<a href="#">Terms</a>
								<a href="#">Privacy policy</a>
								<a href="#">Contact Us</a>
								<a href="#">Careers</a>
							</div>
							<div class="copyright">All right reserved. &copy; <a target="_blank" href="index.php">recovR</a><small>   Your Lost Identifiable Items Recovery Experts</small></div>
						</div>
						<div class="cell-view">
							<div class="social-content">
								<a class="post-facebook" href="https://facebook.com/" target="_blank"><i class="fa fa-facebook"></i></a>
								<a class="post-google-plus" href="#"><i class="fa fa-google-plus"></i></a>
								<a class="post-twitter" href="https://twitter.com/" target="_blank"><i class="fa fa-twitter"></i></a>
								<a class="linkedin" href="#"><i class="fa fa-linkedin"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- footer area end -->
<!-- Back to top Link -->
<div id="to-top" class="main-bg"><span class="fa fa-chevron-up"></span></div>
<!--Jquery-->
	<!--Jquery-->
<script src="vendor/jquery/jquery-3.2.1.min.js" ></script>
<script src="vendor/hash/hash_functions.js" type="text/javascript" ></script>
<script src="assets/js/bootbox.min.js"></script>
<!-- Bootstrap Javascript -->
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/smoothscroll.js"></script>
<script src="assets/js/wow.min.js"></script>
<!-- Mynav Bar Script -->
<script src="assets/js/app.min.js"></script>
<!-- Custom Javascript -->
<script src="assets/js/custom.js"></script>
</body>
</html>
<!-- Localized -->
