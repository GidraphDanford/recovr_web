<?php
include_once "php/session_manager.php";
	// print_r ($_SESSION);
	if(isset($_SESSION['user_name'])){
		header('Location: index.php',true,301);
	}
?>
<!DOCTYPE HTML>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="author" content="iglyphic">
	<link rel="shortcut icon" href="assets/img/logo.png">
	<title>recovR - Register</title>
		<!--Jquery-->
	<script src="vendor/jquery/jquery-3.2.1.min.js" ></script>
	<script src="vendor/hash/hash_functions.js" type="text/javascript" ></script>
	<script src="assets/js/bootbox.min.js"></script>

	<!-- Bootstrap Javascript -->
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/smoothscroll.js"></script>
	<script src="assets/js/wow.min.js"></script>
	<!-- Bootstrap css -->
	<link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/css/bootstrapValidator.min.css"/>
  <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.min.js"> </script>
	<!-- Mynav Bar Script -->
	<!-- Custom Javascript -->
	<script src="assets/js/custom.js"></script>
	<!-- reset CSS -->
	<link href="assets/css/reset.css" rel="stylesheet">
	<!-- icon CSS -->
	<link href="assets/css/fontello.css" rel="stylesheet" type="text/css">
	<link href="assets/css/font-awesome.min.css" rel="stylesheet">
	<!-- Main css -->
	<link href="assets/css/main.css" rel="stylesheet" type="text/css">
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body class="style-2 nav-on-header login-page">
<!-- Start Navigation bar -->
<nav class="navbar">
	<div class="container">
		<!-- Logo -->
		<div class="pull-left">
			<a class="navbar-toggle" href="#" data-toggle="offcanvas">
				<i class="ti-menu"></i>
			</a>
			<div class="logo-wrapper">
				<a class="logo" href="index.php">
					<img src="assets/img/logo.png" width="63px" height="60px" alt="logo">
				</a>
				<a class="logo-alt" href="index.php">
					<img src="assets/img/logo.png" alt="logo-alt">
				</a>
			</div>
		</div>
		<!-- END Logo -->
		<!-- User account -->
		<div class="pull-right user-login">
			<a class="btn btn-sm btn-primary" href="Login.php">Login</a> or &nbsp;
			<a href="Register.php">Register</a>
		</div>
		<!-- END User account -->
		<!-- Navigation menu -->
		<ul class="nav-menu user-login">
			<li>
				<a  href="index.php">Home</a>
			</li>
			<li>
				<a   href="Download.php">Download</a>
			</li>
			<li>
				<a class="active" href="Report.php">Report</a>
			</li>
			<li>
				<a  href="About.php">About Us</a>
			</li>
			<li>
				<a  href="Contact.php">Contact Us</a>
			</li>
		</ul>
		<!-- END Navigation menu -->
	</div>
</nav>
<!-- End Navigation bar -->
<!-- Start Pages Title  -->
<section id="page-title" class="page-title-style2">
	<div class="color-overlay"></div>
	<div class="container inner-img">
		<div class="row">
			<div class="Page-title">
				<div class="col-md-12 text-center">
					<div class="title-text">
						<h3 style="color:#fff;" class="page-title">recovR - Register</h3>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- End Pages Title  -->
<script type="text/javascript">
$(document).ready(function() {
    $('#registerForm').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            name: {
                validators: {
                    notEmpty: {
                        message: "Please provide your name"
                    }
                }
            },
            email: {
                validators: {
                    notEmpty: {
                        message: "Please provide your email address"
                    }
                }
            },
            phone_no: {
                validators: {
                    notEmpty: {
                        message: "Please provide your phone number"
                    }
                }
            },
            password: {
                validators: {
                    notEmpty: {
                        message: "Please provide a password"
                    }
                }
            },
            confPass: {
                validators: {
                    notEmpty: {
                        message: "Please confirm your password"
                    },
                    identical: {
                    	field: 'password',
                    	message: "Passwords do not match"
                    }
                }
            }
        }
    })
});

</script>
<!-- Content Start -->
<main class="main-content">
<div class="login-block">
		<img src="assets/img/logo_.png" alt="">
		<h1>Register into recovR</h1>
		<form id="registerForm">
			<div class="form-group">
				<div class="input-group">
					<span class="input-group-addon"><i class="ti-user"></i></span>
					<input type="text" id="name" name="name" class="form-control" placeholder="Your name">
				</div>
			</div>
			<hr class="hr-xs">
			<div class="form-group">
				<div class="input-group">
					<span class="input-group-addon"><i class="ti-email"></i></span>
					<input type="email" id="email" name="email" class="form-control" placeholder="Your email address">
				</div>
			</div>
			<hr class="hr-xs">
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon"><i class="ti-tablet"></i></span>
						<input type="number" id="phone_no" name="phone_no" class="form-control" placeholder="254700000000">
					</div>
				</div>
			<hr class="hr-xs">
			<div class="form-group">
				<div class="input-group">
					<span class="input-group-addon"><i class="ti-unlock"></i></span>
					<input type="password" id="password" name="password" class="form-control" placeholder="Choose a password">
				</div>
			</div>
			<hr class="hr-xs">
			<div class="form-group">
				<div class="input-group">
					<span class="input-group-addon"><i class="ti-unlock"></i></span>
					<input type="password" id="confPass" name="confPass" class="form-control" placeholder="Confirm password">
				</div>
			</div>

		</form>
		<div id="register_loading" >

		</div>
		<button class="btn btn-primary btn-block" onclick="javascript:signupUser()" type="submit">Sign up</button>
	</div>
	<div class="login-links">
		<p class="text-center">Already have an account? <a class="txt-brand" href="login">Login</a></p>
	</div>
</main>
<!-- Content End -->
<!-- start footer area -->
<section id="index-footer" class="footer-area-content">
	<!-- Newsletter -->
	<div id="newsletter">
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-center">
					<h3><i class="fa fa-envelope-o"></i>Keep in touch, Join our newsletter</h3>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-md-offset-3">
					<div class="col-sm-8">
						<input type="email" required="required" placeholder="Your Email Address" id="subscription_email" class="form-control" name="subscription_email">
					</div>
					<div class="col-sm-4">
						<a href="#" class="btn btn-subscribe">Subscribe</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- END: Newsletter -->
	<div class="footer-bottom footer-wrapper style-3">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="footer-bottom-navigation">
						<div class="cell-view">
							<div class="footer-links">
								<a href="#">Site Map</a>
								<a href="#">Search</a>
								<a href="#">Terms</a>
								<a href="#">Privacy policy</a>
								<a href="#">Contact Us</a>
								<a href="#">Careers</a>
							</div>
							<div class="copyright">All right reserved. &copy; <a target="_blank" href="index.php">recovR</a><small>   Your Lost Identifiable Items Recovery Experts</small></div>
						</div>
						<div class="cell-view">
							<div class="social-content">
								<a class="post-facebook" href="https://facebook.com/" target="_blank"><i class="fa fa-facebook"></i></a>
								<a class="post-google-plus" href="#"><i class="fa fa-google-plus"></i></a>
								<a class="post-twitter" href="https://twitter.com/" target="_blank"><i class="fa fa-twitter"></i></a>
								<a class="linkedin" href="#"><i class="fa fa-linkedin"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- footer area end -->
<!-- Back to top Link -->
<div id="to-top" class="main-bg"><span class="fa fa-chevron-up"></span></div>
</body>

<script src="https://www.gstatic.com/firebasejs/4.12.1/firebase.js"></script>
<script>   // Initialize Firebase
	var config = {
	apiKey: "AIzaSyBMbSNheWTyDk3slRCxO5diLxTynX7Q64Q",
	authDomain: "recovr-app.firebaseapp.com",
	databaseURL: "https://recovr-app.firebaseio.com",
	projectId: "recovr-app",
	storageBucket: "recovr-app.appspot.com",
	messagingSenderId: "327082146500"   };

	firebase.initializeApp(config);

	function replaceBulk( str, findArray, replaceArray ){
		var i, regex = [], map = {};
		for( i=0; i<findArray.length; i++ ){
			regex.push( findArray[i].replace(/([-[\]{}()*+?.\\^$|#,])/g,'\\$1') );
			map[findArray[i]] = replaceArray[i];
		}
		regex = regex.join('|');
		str = str.replace( new RegExp( regex, 'g' ), function(matched){
			return map[matched];
		});
		return str;
	}
	function signupUser(){
		var full_name = $('#name').val()
		var email = $('#email').val()
		var phone_no = $('#phone_no').val()
		var password = $('#password').val()
		var confirm_pass = $('#confPass').val()

		// console.log(`fullname: ${full_name}\nfullname: ${email}\nfullname: ${phone_no}\nfullname: ${password}\nfullname: ${confirm_pass}`)
		if(full_name == ''){
			console.log('Missing Name')
		}else if(email == ''){
			console.log('Missing email')
		}else if(phone_no == ''){
			console.log('Missing phone')
		}else if(password == ''){
			console.log('Missing password')
		}else if(confirm_pass == ''){
			console.log('Missing confirmation password')
		}else if(password != confirm_pass){
			console.log('Mismatching password and confirmation password')
		}else{
			var fb_phone_number = phone_no.replace('254','0')
			$('#register_loading').append('<Small class="text text-md" >Logging In...</Small>')
			firebase.auth().createUserWithEmailAndPassword(email,password).then(()=>{
				var userhash = b64_sha1(email.toUpperCase())
				// console.log(`userhash: ${userhash}\n joined name: ${newJoinedName}`)
				// console.log(userhash);
				var userhash = userhash.split('/').join('_')
				var userhash = userhash.split('#').join('_')
				var userhash = userhash.split('.').join('_')
				var userhash = userhash.split('[').join('_')
				var userhash = userhash.split(']').join('_')
				var userhash = userhash.split('$').join('_')
				// console.log(userhash);
				firebase.database().ref('users/'+userhash+'/').update({
					user_name: full_name.toUpperCase(),
					user_email: email.toLowerCase(),
					user_phone: fb_phone_number,
				}).then(()=>{
					window.location.href = 'Login.php'
				}).catch((err)=>{
					console.log(err.message)
					$('#register_loading').empty()
					alert('user details update error: '+err.message)
				})
			}).catch((err)=>{
				console.log(err.message)
				$('#register_loading').empty()
				alert('user creation error: '+err.message)
			})
		}
	}
</script>
</html>
<!-- Localized -->
