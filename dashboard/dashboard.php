<?php
include_once "../php/session_manager.php";
	// print_r ($_SESSION);
	if(!isset($_SESSION['user_name'])){
		header('Location: http://localhost/recovr/',true,301);
	}
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>RecovR Dashboard</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Paper Dashboard core CSS    -->
    <link href="assets/css/paper-dashboard.css" rel="stylesheet"/>


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="assets/css/main.css" rel="stylesheet" />
		<link rel="stylesheet" href="assets/css/process.css">


    <!--  Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
    <link href="assets/css/themify-icons.css" rel="stylesheet">

</head>
<body>

<div class="wrapper">
    <div class="sidebar" data-background-color="black" data-active-color="danger">
    	<div class="sidebar-wrapper">
            <div class="logo">
                <a href="http://localhost/recovr/" class="simple-text">
                    <img src="../assets/img/logo.png" alt="logo" style="width:80%;">
                </a>
            </div>

            <ul class="nav">
                <li class="active">
                    <a href="dashboard.php">
                        <i class="ti-panel"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <li>
                    <a href="profile.php">
                        <i class="ti-user"></i>
                        <p>Profile</p>
                    </a>
                </li>

                <li>
                    <a href="items.php">
                        <i class="ti-pencil-alt2"></i>
                        <p>Items</p>
                    </a>
                </li>
								<li class="active-pro">
                    <a href="http://localhost/recovr/">
                        <i class="fa fa-lg fa-arrow-left"></i>
                        <p>Back to Home</p>
                    </a>
                </li>
            </ul>
    	</div>
    </div>

    <div class="main-panel">
        <nav class="navbar navbar-default" style="background:rgba(10,10,10,.9);" >
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar bar1"></span>
                        <span class="icon-bar bar2"></span>
                        <span class="icon-bar bar3"></span>
                    </button>
                    <a class="navbar-brand" style="color:#daa521;" href="#">Dashboard</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="ti-bell"></i>
                                <p class="notification"></p>
																<p style="color:#daa521;" >Notifications</p>
																<b class="caret"></b>
                              </a>
                              <ul class="dropdown-menu">
                              </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>


        <div class="content" style="background:url('assets/img/dash_bg.jpg'); background-size:cover; " >
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-3 col-sm-6">
                        <div class="card">
                            <div class="content">
                                <div class="row">
                                    <div class="col-xs-5">
                                        <div class="icon-big icon-warning text-center">
                                            <i class="ti-server"></i>
                                        </div>
                                    </div>
                                    <div class="col-xs-7">
                                        <div class="numbers">
                                            <p>Total Items</p>
                                            5
                                        </div>
                                    </div>
                                </div>
                                <div class="footer">
                                    <hr />
                                    <div class="stats">
                                        <i class="ti-reload"></i> Update now
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="card">
                            <div class="content">
                                <div class="row">
                                    <div class="col-xs-5">
                                        <div class="icon-big icon-success text-center">
                                            <i class="fa fa-low-vision fa-lg"></i>
                                        </div>
                                    </div>
                                    <div class="col-xs-7">
                                        <div class="numbers">
                                            <p>Lost Items</p>
                                            0
                                        </div>
                                    </div>
                                </div>
                                <div class="footer">
                                    <hr />
                                    <div class="stats">
                                         <i class="ti-reload"></i> Update now
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="card">
                            <div class="content">
                                <div class="row">
                                    <div class="col-xs-5">
                                        <div class="icon-big icon-danger text-center">
                                            <i class="fa fa-cart-arrow-down fa-lg"></i>
                                        </div>
                                    </div>
                                    <div class="col-xs-7">
                                        <div class="numbers">
                                            <p>Found Items</p>
                                            3
                                        </div>
                                    </div>
                                </div>
                                <div class="footer">
                                    <hr />
                                    <div class="stats">
                                        <i class="ti-reload"></i> Update now
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="card">
                            <div class="content">
                                <div class="row">
                                    <div class="col-xs-5">
                                        <div class="icon-big icon-info text-center">
                                            <i class="fa fa-lg fa-qrcode"></i>
                                        </div>
                                    </div>
                                    <div class="col-xs-7">
                                        <div class="numbers">
                                            <p>QR Scans</p>
                                            23
                                        </div>
                                    </div>
                                </div>
                                <div class="footer">
                                    <hr />
                                    <div class="stats">
                                        <i class="ti-reload"></i> Update now
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Why recovR</h4>
                                <p class="category">Our process at a glance</p>
																<section class="order-process-step">
																	<div class="container frontend">
																		<div class="clearfix"></div>
																		<!--Facts-->
																		<!--Process Block-->
																		<div class="block process-block">
																			<!--<h2>The process</h2>
																							<h5>4 Steps for Success</h5>-->
																			<div class="row text-center">
																				<ol class="process">
																					<li class="col-md-3 col-sm-6 col-xs-12">
																						<i class="fa fa-eye" aria-hidden="true"></i>
																						<h4>Find an Item</h4>
																					</li>

																					<li class="col-md-3 col-sm-6 col-xs-12">
																						<div style="display:flex;flex-direction:row">
																							<i style="flex:1;margin-right:3px" class="fa fa-frown-o"></i>
																							<i style="flex:1;margin-right:3px" class="fa fa-search" ></i>
																							<i style="flex:1;margin-right:3px" class="fa fa-qrcode"></i>

																						</div>
																						<h4>Search a Lost Item</h4>
																					</li>

																					<li class="col-md-3 col-sm-6 col-xs-12">
																						<div style="display:flex;flex-direction:row">
																							<i style="flex:1;margin-right:3px" class="fa fa-phone" ></i>
																							<i style="flex:1;margin-right:3px" class="fa fa-qrcode"></i>
																							<i style="flex:1;margin-right:3px" class="fa fa-desktop"></i>
																						</div>

																						<h4>Report to Us</h4>
																					</li>

																					<li class="col-md-3 col-sm-6 col-xs-12">
																						<div style="display:flex;flex-direction:row">
																							<i style="flex:1;margin-right:3px" class="fa fa-suitcase"></i>
																							<i style="flex:1;margin-right:3px" class="fa fa-truck" ></i>
																							<i style="flex:1;margin-right:3px" class="fa fa-thumbs-o-up"></i>

																						</div>
																						<h4>Get your Item!</h4>
																					</li>
																				</ol>
																			</div>
																			<div class="clearfix"></div>
																		</div>
																		<!--/Process Block-->
																		<div class="clearfix"></div>
																		<!--/Facts-->
																	</div>
																</section>
                            </div>
                            <div class="content">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <footer class="footer">
            <div class="container-fluid">
                <nav class="pull-left">
                    <ul>

                        <li>
                            <a href="http://localhost/recovr/">
                                RecovR
                            </a>
                        </li>
                        <li>
                            <a href="http://localhost/recovr/contact.php">
                               Contact
                            </a>
                        </li>
                        <li>
                            <a href="http://localhost/recovr/about.php">
                                About Us
                            </a>
                        </li>
                    </ul>
                </nav>
                <div class="copyright pull-right">
                    &copy; <script>document.write(new Date().getFullYear())</script>, made with <i class="fa fa-heart heart"></i> by <a href="http://localhost/recovr/">RecovR</a>
                </div>
            </div>
        </footer>

    </div>
</div>


</body>

    <!--   Core JS Files   -->
  <script src="assets/js/jquery-3.2.1.min.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="assets/js/bootstrap-checkbox-radio.js"></script>

	<!--  Charts Plugin -->
	<script src="assets/js/chartist.min.js"></script>

    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>

    <!-- Paper Dashboard Core javascript and methods for Demo purpose -->
	<script src="assets/js/paper-dashboard.js"></script>
	<!-- Paper Dashboard DEMO methods, don't include it in your project! -->
	<script src="assets/js/dash.js"></script>

	<script type="text/javascript">
    	$(document).ready(function(){

    	});
	</script>

</html>
