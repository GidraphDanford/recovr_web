<?php
include_once "../php/session_manager.php";
if(!isset($_SESSION['user_name'])){
  header('Location: http://localhost/recovr/',true,301);
}
include_once('../assets/php/vendor/unirest/Unirest.php');
$json = Unirest\Request::post("https://andruxnet-random-famous-quotes.p.mashape.com/?cat=famous",
  array(
    "X-Mashape-Key" => "wK0n0U0lmqmshL5zYIuwMYQsIRrCp1zH6ltjsnx871O0m0bni6",
    "Content-Type" => "application/x-www-form-urlencoded",
    "Accept" => "application/json"
  )
);

$quotable = $json->body->quote;
$author = $json->body->author;
 ?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>RecovR Dashboard</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Paper Dashboard core CSS    -->
    <link href="assets/css/paper-dashboard.css" rel="stylesheet"/>

    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="assets/css/main.css" rel="stylesheet" />

    <!--  Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
    <link href="assets/css/themify-icons.css" rel="stylesheet">

</head>
<body>

<div class="wrapper">
	<div class="sidebar" data-background-color="black" data-active-color="danger">
		<div class="sidebar-wrapper">
					<div class="logo">
							<a href="http://localhost/recovr/" class="simple-text">
									<img src="../assets/img/logo.png" alt="logo" style="width:80%;">
							</a>
					</div>

					<ul class="nav">
							<li >
									<a href="dashboard.php">
											<i class="ti-panel"></i>
											<p>Dashboard</p>
									</a>
							</li>
							<li class="active" >
									<a href="profile.php">
											<i class="ti-user"></i>
											<p>Profile</p>
									</a>
							</li>

							<li>
									<a href="items.php">
											<i class="ti-pencil-alt2"></i>
											<p>Items</p>
									</a>
							</li>
							<li class="active-pro">
									<a href="http://localhost/recovr/">
											<i class="fa fa-lg fa-arrow-left"></i>
											<p>Back to Home</p>
									</a>
							</li>
					</ul>
		</div>
  </div>

    <div class="main-panel">
			<nav class="navbar navbar-default" style="background:rgba(10, 10, 10, .9);" >
					<div class="container-fluid">
							<div class="navbar-header">
									<button type="button" class="navbar-toggle">
											<span class="sr-only">Toggle navigation</span>
											<span class="icon-bar bar1"></span>
											<span class="icon-bar bar2"></span>
											<span class="icon-bar bar3"></span>
									</button>
									<a class="navbar-brand" style="color:#daa521;" href="#">Your Profile</a>
							</div>
							<div class="collapse navbar-collapse">
									<ul class="nav navbar-nav navbar-right">
											<li class="dropdown">
														<a href="#" class="dropdown-toggle" data-toggle="dropdown">
															<i class="ti-bell"></i>
															<p class="notification"></p>
															<p style="color:#daa521;" >Notifications</p>
															<b class="caret"></b>
														</a>
														<ul class="dropdown-menu">
														</ul>
											</li>
									</ul>
							</div>
					</div>
			</nav>


			<div class="content" style="background:url('assets/img/dash_bg.jpg'); background-size:cover; " >
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-4 col-md-5">
                        <div class="card card-user">
                            <div class="image">
                                <img src="assets/img/background.jpg" alt="..."/>
                            </div>
                            <div class="content">
                                <div class="author">
                                  <img class="avatar border-white" src="assets/img/faces/face-2.jpg" alt="..."/>
                                  <h4 class="title">Dunford Mainor<br />
                                     <a href="#"><small>@dunmainor</small></a>
                                  </h4>
                                </div>
                                <p class="description text-center">
                                    <p ><?php echo $quotable; ?></p>
																		<small><?php echo $author; ?></small>
                                </p>
                            </div>
                            <hr>
                            <div class="text-center">
                                <div class="row">
                                    <div class="col-md-3 col-md-offset-1">
                                        <h5>5<br /><small>Items</small></h5>
                                    </div>
                                    <div class="col-md-4">
                                        <h5>0<br /><small>Lost</small></h5>
                                    </div>
                                    <div class="col-md-3">
                                        <h5>23<br /><small>Scans</small></h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-7">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Your Profile</h4>
                            </div>
                            <div class="content">
                                <form>
                                    <div class="row">
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label>Company</label>
                                                <input type="text" class="form-control border-input" readonly placeholder="(optional) Company" value="Stackstech Inc">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Username</label>
                                                <input type="text" class="form-control border-input" placeholder="Username" value="DunMa">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Email address</label>
                                                <input readonly type="email" class="form-control border-input" placeholder="Email" value="danmainor@outlook.com">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>First Name</label>
                                                <input readonly type="text" class="form-control border-input" placeholder="Company" value="Dunford">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Last Name</label>
                                                <input readonly type="text" class="form-control border-input" placeholder="Last Name" value="Mainor">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Address</label>
                                                <input type="text" class="form-control border-input" placeholder="Home Address" value="1234, WestStreet">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
																			<div class="col-md-4">
																					<div class="form-group">
																							<label>County</label>
																							<input type="text" class="form-control border-input" placeholder="County" value="Kirinyaga">
																					</div>
																			</div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>City</label>
                                                <input type="text" class="form-control border-input" placeholder="City" value="Kirinyaga">
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Postal Code</label>
                                                <input type="number" class="form-control border-input" placeholder="Postal Code" value="00253">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>About Me</label>
                                                <textarea rows="5" class="form-control border-input" placeholder="Here can be your description" value="Mike">A biography, or simply bio, is a detailed description of a person's life. It involves more than just the basic facts like education, work, relationships, and death; it portrays a person's experience of these life events.
																								</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text-center">
                                        <button type="submit" class="btn btn-info btn-fill btn-wd">Update Profile</button>
                                    </div>
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
      </div>

			<footer class="footer">
					<div class="container-fluid">
							<nav class="pull-left">
									<ul>

											<li>
													<a href="http://localhost/recovr/">
															RecovR
													</a>
											</li>
											<li>
													<a href="http://localhost/recovr/contact.php">
														 Contact
													</a>
											</li>
											<li>
													<a href="http://localhost/recovr/about.php">
															About Us
													</a>
											</li>
									</ul>
							</nav>
							<div class="copyright pull-right">
									&copy; <script>document.write(new Date().getFullYear())</script>, made with <i class="fa fa-heart heart"></i> by <a href="http://localhost/recovr/">RecovR</a>
							</div>
					</div>
			</footer>

    </div>
</div>


</body>

    <!--   Core JS Files   -->
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="assets/js/bootstrap-checkbox-radio.js"></script>

	<!--  Charts Plugin -->
	<script src="assets/js/chartist.min.js"></script>

    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>

    <!--  Google Maps Plugin    -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>

    <!-- Paper Dashboard Core javascript and methods for Demo purpose -->
	<script src="assets/js/paper-dashboard.js"></script>

	<!-- Paper Dashboard DEMO methods, don't include it in your project! -->
	<script src="assets/js/demo.js"></script>

</html>
