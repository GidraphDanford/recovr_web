<?php
include_once "php/session_manager.php";
	// print_r ($_SESSION);
if(isset($_GET['filter_button'])){
	header("Location: Results.php ",true);
}
?>
<!DOCTYPE HTML>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="author" content="iglyphic">
	<link rel="shortcut icon" href="assets/img/logo.png">
	<title>recovR Documents Centre - Home</title>
	<!-- reset CSS -->
	<link href="assets/css/reset.css" rel="stylesheet" type="text/css">
	<!-- Bootstrap css -->
	<link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<!-- icon CSS -->
	<link href="assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<!-- Main css -->
	<link href="assets/css/main.css" rel="stylesheet" type="text/css">
	<!--Jquery-->
	<script src="vendor/jquery/jquery-3.2.1.min.js" ></script>
	<script src="vendor/hash/hash_functions.js" type="text/javascript" ></script>
	<!-- Bootstrap Javascript -->
	<script src="assets/js/bootstrap.min.js"></script>
	<!-- Mynav Bar Script -->
	<script src="assets/js/app.min.js"></script>
	<!-- Custom Javascript -->
	<script src="assets/js/smoothscroll.js"></script>
	<script src="assets/js/wow.min.js"></script>
	<script src="assets/js/custom.js"></script>
	<script src="assets/js/bootbox.min.js"></script>
	<script src="assets/js/bootstrapValidator.js"></script>
</head>
<body class="style-2 nav-on-header">
	<!-- Start Loading -->
	<section class="loading-overlay">
		<div class="Loading-Page">
			<h1 class="loader">Loading...</h1>
		</div>
	</section>
	<!-- End Loading  -->
	<!-- Start Navigation bar -->
	<nav class="navbar">
		<div class="container">
			<!-- Logo -->
			<div class="pull-left">
				<a class="navbar-toggle" href="#" data-toggle="offcanvas">
					<i class="ti-menu"></i>
				</a>
				<div class="logo-wrapper">
					<a class="logo" href="index.php">
						<img src="assets/img/logo.png" width="63px" height="60px" alt="logo">
					</a>
					<a class="logo-alt" href="index.php">
						<img src="assets/img/logo.png" alt="logo-alt">
					</a>
				</div>
			</div>
			<!-- END Logo -->
			<!-- User account -->
			<!-- User account -->
			<div class="pull-right user-login">
				<?php if(isset($_SESSION['user_name'])){
					echo '<a class="btn btn-sm btn-primary" href="Logout.php">Logout</a>';
				}else{
					echo '<a class="btn btn-sm btn-primary" href="Login.php">Login</a> or &nbsp;
								<a href="Register.php">Register</a>';
				} ?>

			</div>
			<!-- END User account -->
			<!-- END User account -->

			<!-- Navigation menu -->
			<ul class="nav-menu user-login">
				<?php if(isset($_SESSION['user_name'])){
					echo '<li><a href="dashboard/dashboard.php">Dashboard</a></li>';
				} ?>
				<li>
					<a class="active" href="index.php">Home</a>
				</li>
	      <li>
					<a   href="Download.php">Download</a>
				</li>
				<li>
					<a href="Report.php">Report</a>
				</li>
	      <li>
					<a href="About.php">About Us</a>
				</li>
				<li>
					<a href="Contact.php">Contact Us</a>
				</li>
			</ul>
			<!-- END Navigation menu -->
		</div>
	</nav>
	<!-- End Navigation bar -->
	<!-- Content Start -->
	<!-- start static header -->
	<section class="static-section">
		<div class="container">
			<div class="row static-header-content-wrapper">
				<div class="col-md-12 static-header-content">
					<div class="static-header-text">
						<h3 style="text-align: center; text-transform: uppercase; margin-bottom: 80px;" class="white">YOU LOSE IT, WE FIND IT!</h3>
					</div>
					<div class="row">
						<div class="col-md-8 col-md-offset-2">
							<form  id="searchForm" class="clearfix" method="get">
								<div class="col-sm-8" style="display:flex; flex-direction:row;">
									<input style="height:56px; flex:5; margin-right:10px" type="text" placeholder="Search by item keyword. e.g. A12354G / 30256884" name="item_key" id="email"
									 class="form-control">
									 <div class=""  style="flex:2;height:56px;margin-top:-20px;" >
										 <small style="height:12px;" >Apply filter</small>
  									 <select type="button" name="filter_button" value="" class="form-control"  style="height:56px;">
  										 <optgroup label="Filter By Category">
  											<option value="Academic Certificates">Certificates</option>
  											<option value="Passport">Passport</option>
											<option value="National ID">Identity Card</option>
											<option value="Tittle Deed">Title Deed</option>
											<option value="Logbook">Logbook</option>
											<option value="Driving License">Driving License</option>
											<option value="Bank Card">Bank Cards</option>
											<option value="N.H.I.F Card">N.H.I.F Card</option>
											<option value="N.S.S.F Card">N.S.S.F Cards</option>
											<option value="Student ID">Student ID</option>
											<option value="Other">Other</option>
  										 </optgroup>
  									 </select>
									 </div>

								</div>
								<div class="col-sm-4">
									<input style="width:100%;height:56px;line-height:56px;font-size:14px;text-transform:capitalize;box-shadow:none;border:none" class="search-form-submit bgred-1 white" type="submit" value="FIND IT NOW!" onclick="javascript:search()" />
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- end static header -->

	<section class="bt-block-home-parallax" style="background-image: url(assets/img/reparallax-one.jpg);">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="lookbook-product">
						<h2>recovR - A one stop items recovery centre.</h2>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- /.bt-block-home-parallax -->
	<!-- start featured restaurant -->
	<div class="featured-product">
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-center">
					<h2 class="font-lg">Find and report
						<span class="font-semibold chef-red">All Docs & Other Items</span> now</h2>
				</div>
				<!-- col-md-12 -->
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="new-restaurant text-center">
						<div class="col-md-3 col-sm-6 padding-reset">
							<img src="assets/img/deed.jpg" alt="Title Deeds">
							<span>
								<h4>Tittle Deeds</h4>
							</span>
						</div>
						<div class="col-md-3 col-sm-6 padding-reset">
							<img style="filter:blur(5px)" src="assets/img/id.jpg" alt="National ID">
							<span>
								<h4>National IDs</h4>
							</span>
						</div>
						<div class="col-md-3 col-sm-6 padding-reset">
							<img src="assets/img/passport.jpg" alt="Passports">
							<span>
								<h4>Passports</h4>
							</span>
						</div>
						<div class="col-md-3 col-sm-6 padding-reset">
							<img src="assets/img/dl.jpg" alt="Driving Licenses">
							<span>
								<h4>Driving Licences</h4>
							</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- featured restaurant Ends -->

	<!-- start footer area -->
	<section id="index-footer" class="footer-area-content">
		<!-- Newsletter -->
		<div id="newsletter">
			<div class="container">
				<div class="row">
					<div class="col-md-12 text-center">
						<h3>
							<i class="fa fa-envelope-o"></i>Keep in touch, Join our newsletter</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6 col-md-offset-3">
						<div class="col-sm-8">
							<input type="email" required="required" placeholder="Your Email Address" id="email" class="form-control" name="email">
						</div>
						<div class="col-sm-4">
							<a href="#" class="btn btn-subscribe">Subscribe</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- END: Newsletter -->
		<div class="footer-bottom footer-wrapper style-3">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="footer-bottom-navigation">
							<div class="cell-view">
								<div class="footer-links">
									<a href="#">Site Map</a>
									<a href="#">Search</a>
									<a href="#">Terms</a>
									<a href="#">Privacy policy</a>
									<a href="#">Contact Us</a>
									<a href="#">Careers</a>
								</div>
								<div class="copyright">All right reserved. &copy; <a target="_blank" href="index.php">recovR</a><small>   Your Lost Identifiable Items Recovery Experts</small></div>
							</div>
							<div class="cell-view">
								<div class="social-content">
									<a class="post-facebook" href="https://facebook.com/" target="_blank">
										<i class="fa fa-facebook"></i>
									</a>
									<a class="post-google-plus" href="#">
										<i class="fa fa-google-plus"></i>
									</a>
									<a class="post-twitter" href="https://twitter.com/" target="_blank">
										<i class="fa fa-twitter"></i>
									</a>
									<a class="linkedin" href="#">
										<i class="fa fa-linkedin"></i>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- footer area end -->
	<!-- Back to top Link -->
	<div id="to-top" class="main-bg">
		<span class="fa fa-chevron-up"></span>
	</div>
	<script src="https://www.gstatic.com/firebasejs/4.12.1/firebase.js"></script>
	<script>   // Initialize Firebase
		var config = {
		apiKey: "AIzaSyBMbSNheWTyDk3slRCxO5diLxTynX7Q64Q",
		authDomain: "recovr-app.firebaseapp.com",
		databaseURL: "https://recovr-app.firebaseio.com",
		projectId: "recovr-app",
		storageBucket: "recovr-app.appspot.com",
		messagingSenderId: "327082146500"   }

		firebase.initializeApp(config)
		console.log('====================================')
		console.log('firebase user')
		console.log('====================================')
		console.log(firebase.auth())
		var login_reg = '<a class="btn btn-sm btn-primary" href="Login.php">Login</a> or &nbsp;'+
						'<a href="Register.php">Register</a>'
		var logout = '<a class="btn btn-sm btn-primary" href="Login.php">Login</a> or &nbsp;'
	
	function search(){
		window.location.href = 'Results.php'
	}

	</script>
</body>
</html>
<!-- Localized -->
